﻿using System;

namespace BrightSpark.Validator.AspNetCoreExt
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Parameter, AllowMultiple = true, Inherited = true)]
    public class ValidateWithAttribute : Attribute
    {
        public Type ValidatorType { get; set; }

        public ValidateWithAttribute(Type validatorType)
        {
            ValidatorType = validatorType;
        }
    }
}
