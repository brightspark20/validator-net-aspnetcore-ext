﻿using System;
using System.Linq;

#if (NETSTANDARD2_0)
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
#endif

#if NET452
using System.Web.Mvc;

#endif

namespace BrightSpark.Validator.AspNetCoreExt
{
    public class ValidateWithActionFilterAttribute : ActionFilterAttribute
    {
        private ActionExecutingContext _context;

#if NET452
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            _context = context;

            var parameters = context.ActionDescriptor.GetParameters();
            foreach (var prop in parameters)
            {
                var validators = GetValidators(prop);
                if (validators.Length == 0)
                {
                    continue;
                }

                var model = context.ActionParameters[prop.ParameterName];
                foreach (var validator in validators)
                {
                    Validate(validator, model);
                }
            }
        }

        private IValidator[] GetValidators(ParameterDescriptor prop)
        {
            return prop.GetCustomAttributes(true)
                .OfType<ValidateWithAttribute>()
                .Select(x => GetValidator(x.ValidatorType))
                .ToArray();
        }

        private void Validate(IValidator validator, object model)
        {
            var summary = validator.Validate(model);
            if (!summary.IsValid)
            {
                foreach (var r in summary.Results)
                {
                    foreach (var e in r.Value.Errors)
                    {
                        _context.Controller.ViewData.ModelState.AddModelError(r.Key, e.Error);
                    }
                }
            }
        }
#endif

#if NETSTANDARD2_0
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            _context = context;

            var parameters = context.ActionDescriptor.Parameters
                .OfType<ControllerParameterDescriptor>();

            foreach (var prop in parameters)
            {
                var validators = GetValidators(prop);
                if (validators.Length == 0)
                {
                    continue;
                }

                var model = context.ActionArguments[prop.Name];
                foreach (var validator in validators)
                {
                    Validate(validator, model);
                }
            }
        }

        private IValidator[] GetValidators(ControllerParameterDescriptor prop)
        {
            return prop.ParameterInfo.GetCustomAttributes(true)
                .OfType<ValidateWithAttribute>()
                .Select(x => GetValidator(x.ValidatorType))
                .ToArray();
        }

        private void Validate(IValidator validator, object model)
        {
            var summary = validator.Validate(model);
            if (!summary.IsValid)
            {
                foreach (var r in summary.Results)
                {
                    foreach (var e in r.Value.Errors)
                    {
                        _context.ModelState.AddModelError(r.Key, e.Error);
                    }
                }
            }
        }

#endif


        private IValidator GetValidator(Type type)
        {
            var validator = Activator.CreateInstance(type) as IValidator;
            return validator;
        }
    }
}