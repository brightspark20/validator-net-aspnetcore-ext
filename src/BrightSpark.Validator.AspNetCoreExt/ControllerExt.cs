#if NETSTANDARD2_0
using Microsoft.AspNetCore.Mvc;
#endif

#if NET452
using System.Web.Mvc;
#endif

namespace BrightSpark.Validator.AspNetCoreExt
{
    public static class ControllerExt
    {
        /// <summary>
        /// Validate model with given validator.
        /// If any validation error occurs, it will be added to model state.
        /// </summary>
        /// <returns>Is model valid</returns>
        public static bool IsValid<T>(this ControllerBase controller, T model, IValidator<T> validator)
        {
            return controller.IsValid(model).By(validator);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IValidatableModelBy<T> IsValid<T>(this ControllerBase controller, T model)
        {
            return new ValidatableModel<T>(controller, model);
        }

        public static IValidatableModelWith<T> Validate<T>(this ControllerBase controller, T model)
        {
            return new ValidatableModel<T>(controller, model);
        }
    }

    public interface IValidatableModelWith<T>
    {
        ValidationSummary With(IValidator<T> validator, bool addErrorsToModelState = true);
    }

    public interface IValidatableModelBy<T>
    {
        bool By(IValidator<T> validator);
    }

    public class ValidatableModel<T> : IValidatableModelWith<T>, IValidatableModelBy<T>
    {
        private readonly ControllerBase _controller;
        private readonly T _model;

        internal ValidatableModel(ControllerBase controller, T model)
        {
            _controller = controller;
            _model = model;
        }

        public bool By(IValidator<T> validator)
        {
            var summary = validator.Validate(_model);
            AddErrorsToModelState(summary);
            return summary.IsValid;
        }

        public ValidationSummary With(IValidator<T> validator, bool addErrorsToModelState = true)
        {
            var summary = validator.Validate(_model);
            if (addErrorsToModelState)
            {
                AddErrorsToModelState(summary);
            }
            return summary;
        }

        private void AddErrorsToModelState(ValidationSummary summary)
        {
#if NET452
            var modelState = _controller.ViewData.ModelState;
#else
            var modelState = _controller.ModelState;
#endif

            foreach (var r in summary.Results)
            {
                foreach (var e in r.Value.Errors)
                {
                    modelState.AddModelError(r.Key, e.Error);
                }
            }
        }
    }
}